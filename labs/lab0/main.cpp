#include <iostream>
#include <string>
#include <array>
#include <vector>

class Vector3 {
public:
    float x;
    float y;
    float z;
    
   // Ve(int r = 0, int i =0)  {real = r;   imag = i;}
    // Constructor
    Vector3(float xx=0, float yy=0, float zz=0){ x=xx; y=yy; z=zz;
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }
    ////geters/////
    float xval() const {return x;}
    float yval() const {return y;}
    float zval() const {return z;}
  
    //setters/////////
    void setx(float f){
        x=f;
    }
    void sety(float f){
        y=f;
    }
    void setz(float f){
        z=f;
    }
 
    void print() {
        
        std::cout << "("<< x << "," << y <<  "," << z << ")" << std::endl;
    }
    float lengthSquared() {
        return x*x+y*y+z*z;
    }
    

    
    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
       std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 add(const  Vector3 &v, const Vector3 &v2) {
    
    Vector3 a(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    
    return a;
    
}
Vector3 operator+(const Vector3 &v,const Vector3 &v2){
    Vector3 c(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    return c;
}

std::ostream& operator << (std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector
    stream << "(" << v.xval() << "," << v.yval() <<","<< v.zval() << ")";
    
    return stream;
}

int main(int argc, char** argv) {
    /*
    std::string s;
    std::cout <<"enter a string"<< std::endl;
    std::cin >> s;
     
     
     std::cout << "hello world " << argv[0] << " " << s << " " << std::endl;
*/
     
    Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);
    Vector3 d = add(a,b);
    Vector3 c = a+b;
    
    std::cout << a+c << std::endl;
    
    Vector3 v;
    v.sety(5);
    
    std::cout << v << std::endl;
    
   // Vector3 array[10];
    /*
    for(int i=0;i<10;i++){
        
        array[i].sety(5);
    }
    
    for(int i=0;i<10;i++){
        
        std::cout<< array[i]<<std::endl;
    }
     */
     
}
