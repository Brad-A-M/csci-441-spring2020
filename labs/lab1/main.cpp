#include <iostream>
#include <algorithm>
#include <initializer_list>

#include "bitmap_image.hpp"


float val(float x, float y, float ax, float ay, float cx, float cy,float bx, float by){
    
    
    float a=(x*(ay-cy)-y*(ax-cx)+ax*cy-ay*cx);
    float b=(bx*(ay-cy)-by*(ax-cx)+ax*cy-ay*cx);
    float al=a/b;
    return al;
}





int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.
     
      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */
    char skip;
    float p1x, p1y, p2x, p2y, p3x, p3y;
    float p1ri, p1gi, p1bi, p2ri, p2gi, p2bi, p3ri, p3gi, p3bi;
    /*
    p1x=50; p1y=50; p1ri=1; p1gi=0; p1bi=0;
    p2x=600; p2y=20; p2ri=0; p2gi=1; p2bi=0;
    p3x=300; p3y=400; p3ri=0; p3gi=0; p3bi=1;
    */
    
    
    
    std::cout <<"Enter 3 points (enter a point as x,y:r,g,b):"<< std::endl;
    std::cout <<"First point:"<< std::endl;
    std::cin >> p1x >> skip >> p1y >> skip >> p1ri >> skip >> p1gi >> skip >> p1bi;
    
    std::cout <<"Point 1 is: ("<< p1x<<","<< p1y<<") : ("<< p1ri<<","<<p1gi<<","<<p1bi<<")"<< std::endl;
    
    std::cout <<"Second point:"<< std::endl;
    std::cin >> p2x >> skip >> p2y >> skip>> p2ri >> skip >> p2gi >> skip >> p2bi;
    
    std::cout <<"Point 2 is: ("<< p2x<<","<< p2y<<") : ("<< p2ri<<","<<p2gi<<","<<p2bi<<")"<< std::endl;
    
    std::cout <<"Thrid point:"<< std::endl;
    std::cin >> p3x >> skip >> p3y >> skip >> p3ri >> skip >> p3gi >> skip >> p3bi;
    
    std::cout <<"Point 3 is: ("<< p3x<<","<< p3y<<") : ("<< p3ri<<","<<p3gi<<","<<p3bi<<")"<< std::endl;
    
    int p1r=p1ri*255;
    int p1g=p1gi*255;
    int p1b=p1bi*255;
    int p2r=p2ri*255;
    int p2g=p2gi*255;
    int p2b=p2bi*255;
    int p3r=p3ri*255;
    int p3g=p3gi*255;
    int p3b=p3bi*255;
    // create an image 640 pixels wide by 480 pixels tall
    
/////Bounding box////
    
    float maxx = std::max({p1x, p2x, p3x});
    float minx = std::min({p1x, p2x, p3x});
    float maxy = std::max({p1y, p2y, p3y});
    float miny = std::min({p1y, p2y, p3y});
   // std::cout << maxx<<" " <<minx<<" "<<maxy<<" "<<miny<< std::endl;
    
    float p1sx=p1x-minx;
    float p2sx=p2x-minx;
    float p3sx=p3x-minx;
    float p1sy=p1y-miny;
    float p2sy=p2y-miny;
    float p3sy=p3y-miny;
    
    //std::cout << p1sx<<" " <<p1sy<<" "<<p2sx<<" "<<p2sy<<" "<<p3sx<<" "<<p3sy<< std::endl;
    int height = maxy-miny;
    int width = maxx-minx;
    
    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);
*/
    bitmap_image image1(width, height);
    
    for (std::size_t y = 0; y < height; ++y)
    {
        for (std::size_t x = 0; x < width; ++x)
        {
            rgb_t colour;
            
            image1.set_pixel(x, y, 255,255,255);
            
        }
    }
    /*
      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.
*/
   
    bitmap_image image2(width, height);
    
    for (std::size_t y = 0; y < height; ++y)
    {
        for (std::size_t x = 0; x < width; ++x)
        {
            float alpha = val(x,y,p1sx,p1sy,p3sx,p3sy,p2sx,p2sy);
            float beta = val(x,y,p1sx,p1sy,p2sx,p2sy,p3sx,p3sy);
            float gamma = 1-alpha-beta;
            rgb_t colour;
            if(alpha<1 && alpha>0 && beta<1 && beta>0 && gamma <1 && gamma>0){
                image2.set_pixel(x, y, 255,255,255);
                
            }
            
        }
    }
    /*
      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */
    bitmap_image image3(width, height);
    
    for (std::size_t y = 0; y < height; ++y)
    {
        for (std::size_t x = 0; x < width; ++x)
        {
            float alpha = val(x,y,p1sx,p1sy,p3sx,p3sy,p2sx,p2sy);
            float beta = val(x,y,p1sx,p1sy,p2sx,p2sy,p3sx,p3sy);
            float gamma = 1-alpha-beta;
            
            int red=gamma*p1r+alpha*p2r+beta*p3r;
            int green=gamma*p1g+alpha*p2g+beta*p3g;
            int blue=gamma*p1b+alpha*p2b+beta*p3b;
            rgb_t colour;
            if(alpha<1 && alpha>0 && beta<1 && beta>0 && gamma <1 && gamma>0){
                image3.set_pixel(x, y, red,green,blue);
                
            }
            
        }
    }
    image1.save_image("square.bmp");
    image2.save_image("triangle.bmp");
    image3.save_image("color.bmp");
    std::cout << "Success" << std::endl;
}
