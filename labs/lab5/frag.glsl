#version 330 core
in vec3 ourColor;
in vec3 ourNormal;
in vec3 FragPos;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform vec3 light;
uniform vec3 eye;

out vec4 fragColor;

void main() {
    
    vec3 lightcolor  = vec3(1.0,1.0,1.0);
    vec3 lightDir = normalize(light - FragPos);
    
    vec3 viewDir = normalize(eye - FragPos);
    vec3 r = reflect(-lightDir, ourNormal);
    int p = 250;
    float spec = pow(max(dot(viewDir, r), 0.0), p);
    vec3 phong = 0.7 * spec * lightcolor;
    
    
    float diff = max(dot(ourNormal, lightDir), 0.0);
    vec3 diffuse = diff * lightcolor;
    
    float ambientStrength = 0.3;
    vec3 ambient = ambientStrength * lightcolor;
    vec3 result = (ambient + diffuse + phong) * ourColor;
    
    
    
    //vec3 result = ambient * ourColor;
    fragColor = vec4(result, 1.0f);
}
