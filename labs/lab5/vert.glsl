#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;


out vec3 ourColor;
out vec3 ourNormal;
out vec3 FragPos;

void main() {
    gl_Position = projection * camera * model * vec4(aPos, 1.0);
    FragPos = vec3(model * vec4(aPos, 1.0));
    vec4 hey= transpose(inverse(model))*vec4 (aNormal,0.0);
    vec3 yo = vec3(hey.x,hey.y,hey.z);
    ourNormal = yo/length(yo);
    //ourNormal = aNormal;
    
    ourColor = aColor;
}
