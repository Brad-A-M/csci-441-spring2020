#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#include "camera.h"
#include "hit.h"
#include "intersector.h"
#include "light.h"
#include "ray.h"
#include "renderer.h"
#include "shape.h"
#include "timer.h"
bool ray_tri(const Ray& ray, const Triangle tri){
    
    glm::vec3 side1 = tri.point2()-tri.point1();
    glm::vec3 side2 = tri.point3()-tri.point1();
    glm::vec3 mal = glm::cross(side1, side2);
    
    double area =abs(mal.length())/2;
    double al,be,ga,area1,area2;
    
    
    glm::vec3 eye = ray.origin;
    glm::vec3 dir = ray.direction;
    
    bool b=0;
    double t;
    if(dot(mal,dir)==0){b=0;}
    else{
        if(mal.x!=0){t=(tri.point1().x-eye.x)/dir.x;}
        else if(mal.y!=0){t=(tri.point1().y-eye.y)/dir.y;}
        else{t=(tri.point1().z-eye.z)/dir.y;}
        
        
        glm::vec3 sdir(t*-1,t*0,t*0);
        glm::vec3 pnt= eye+sdir;
        
        
        
        glm::vec3 bside1 = tri.point2()-pnt;
        glm::vec3 bside2 = tri.point3()-pnt;
        glm::vec3 bmal = glm::cross(bside1, bside2);
        area1 = sqrt(bmal.x*bmal.x+bmal.y*bmal.y+bmal.z*bmal.z)/2;
        
        
        glm::vec3 cside1 = tri.point1()-pnt;
        glm::vec3 cside2 = tri.point3()-pnt;
        glm::vec3 cmal = glm::cross(cside1, cside2);
        area2 = sqrt(cmal.x*cmal.x+cmal.y*cmal.y+cmal.z*cmal.z)/2;
        
        al = area1/area;
        be =area2/area;
        ga =1-al-be;
        
        if(al>0 && al<1 && be <1 && be>0 && ga<1 && ga>0){b=1;}
    }
    
    return b;
}

bool ray_box_pred(const Ray& ray, std::vector<Triangle> box){
    bool b=0;
    
    for (std::vector<Triangle>::iterator it = box.begin(); it != box.end(); ++it){
        if(ray_tri(ray,*it)==1){
            b=1;
            break;
        }
    }
    
    return b;
}

class BruteForceIntersector : public Intersector {
public:

    Hit find_first_intersection(const World& world, const World& world1, const World& world2, const Ray& ray) {
        Hit hit(ray);
        for (auto surface : world.shapes()) {
            double cur_t = surface->intersect(ray);
            
            if (cur_t < hit.t()) {
                hit.update(surface, cur_t);
            }
        }
        return hit;
    }
};


class MySlickIntersector : public Intersector {
public:
    Hit find_first_intersection(const World& world, const World& world1 ,const World& world2, const Ray& ray) {
        Hit hit(ray);
    
            if((ray.eval(100)).x>0){
                for (auto surface : world1.shapes()) {
                    double cur_t = surface->intersect(ray);
                    //std::cout << "hey" << std::endl;
                    if (cur_t < hit.t()) {
                        hit.update(surface, cur_t);
                    }
                }
        
            }
            else{
                for (auto surface : world2.shapes()) {
                    double cur_t = surface->intersect(ray);
                    
                    if (cur_t < hit.t()) {
                        hit.update(surface, cur_t);
                    }
                }
                
            }
        
        
        // TODO: accelerate finding intersections with a spatial data structure.
        return hit;
    }
};


double rand_val() {
    static bool init = true;
    if (init) {
        srand(time(NULL));
        init = false;
    }
    return ((double) rand() / (RAND_MAX));
}

glm::vec3 rand_color() {
    return glm::vec3(rand_val(),rand_val(),rand_val());
}


std::vector<Triangle> random_box() {
    float  x = (rand_val() * 8) - 4;
    float  y = (rand_val() * 8) - 4;
    float  z = rand_val() * 5;
    float scale = rand_val() * 2;
    return Obj::make_box(glm::vec3(x, y, z), scale, rand_color());
}


int main(int argc, char** argv) {

    // set the number of boxes
    int NUM_BOXES = 4;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // setup the camera
    float dist_to_origin = 5;
    Camera camera(
            glm::vec3(0, 0, -dist_to_origin),   // eye
            glm::vec3(0, 0, 0),                 // target
            glm::vec3(0, 1, 0),                 // up
            glm::vec2(-5, -5),                  // viewport min
            glm::vec2(5, 5),                    // viewport max
            dist_to_origin,                     // distance from eye to view plane
            glm::vec3(.3, .6, .8)               // background color
    );

    // setup lights
    // see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
    // for good attenuation value.
    // I found the values at 7 to be nice looking
    PointLight l1(glm::vec3(1, 1, 1), glm::vec3(3, -3, 0), 1.0, .7, 0.18);
    DirectionalLight l2(glm::vec3(.5, .5, .5), glm::vec3(-5, 4, -1));
    Lights lights = { &l1, &l2 };

    // setup world
    World world;
    World world1;
    World world2;

    // add the light
    world.append(Sphere(l1.position(), .25, glm::vec3(1,1,1)));
    world1.append(Sphere(l1.position(), .25, glm::vec3(1,1,1)));

    // and the spheres
    world.append(Sphere(glm::vec3(1, 1, 1), 1, rand_color()));
    world.append(Sphere(glm::vec3(2, 2, 4), 2, rand_color()));
    world.append(Sphere(glm::vec3(3, 3, 6), 3, rand_color()));
    world1.append(Sphere(glm::vec3(1, 1, 1), 1, rand_color()));
    world1.append(Sphere(glm::vec3(2, 2, 4), 2, rand_color()));
    world1.append(Sphere(glm::vec3(3, 3, 6), 3, rand_color()));
    

    // and add some boxes and prep world for rendering
    for (int i = 0 ; i < NUM_BOXES ; ++i) {
        std::vector<Triangle> boox=random_box();
        
        glm::vec3 center((boox.front().point1().x+boox.back().point1().x)/2,(boox.front().point1().y+boox.back().point1().y)/2,(boox.front().point1().z+boox.back().point1().z)/2);
        
        double width = abs(center.x-boox.front().point1().x);
        if(center.x-width>0){world1.append(boox);}
        else if(center.x+width<0){world2.append(boox);}
        else{world1.append(boox);
            world2.append(boox);
        }
        //world.append(boox);
    }
    world.lock();
    world1.lock();
    world2.lock();

    // create the intersector
    //BruteForceIntersector intersector;
    
    // create the intersector
    MySlickIntersector intersector;

    // and setup the renderer
    Renderer renderer(&intersector);

    // render
    Timer timer;
    timer.start();
    renderer.render(image, camera, lights, world, world1, world2);
    timer.stop();

    image.save_image("ray-traced.bmp");
    std::cout << "Rendered in " <<  timer.total() << " milliseconds" << std::endl;
    std::vector<Triangle> testb = random_box();
    Ray testr;
  
    std::cout<< world.shapes().size() << "," << world1.shapes().size()<< "," << world2.shapes().size()<<std::endl;
    /*
    std::vector<Triangle> box1=Obj::make_box(glm::vec3(5,0,0),10,glm::vec3(0,0,0));
    
   std::cout << "point " <<  box1.back().point1().x <<","<<box1.front().point1().y << ","<< box1.front().point1().z << std::endl;
    glm::vec3 center((box1.front().point1().x+box1.back().point1().x)/2,(box1.front().point1().y+box1.back().point1().y)/2,(box1.front().point1().z+box1.back().point1().z)/2);
    
    std::cout << "center " <<  center.x <<","<<center.y << ","<< center.z << std::endl;

    std::cout << "width " << 2*abs(center.x-box1.front().point1().x)<<std::endl;

    
    std::cout << "test predicate " <<  ray_box_pred(testr,testb) << std::endl;
    
    
    std::vector<Triangle> sox=Obj::make_box(glm::vec3(0,0,0),1,glm::vec3(0,0,0));
    

    Triangle tri = sox.front();
    glm::vec3 side1 = tri.point2()-tri.point1();
    glm::vec3 side2 = tri.point3()-tri.point1();
    glm::vec3 mal = glm::cross(side1, side2);
    
    std::cout << "mal vector:" <<  mal.x<<"," << mal.y<<"," <<mal.z << std::endl;
    std::cout << "box vector:" <<  tri.point1().x<<"," << tri.point1().y<<"," <<tri.point1().z <<") -*- (" << tri.point2().x<<"," << tri.point2().y << ","<<tri.point2().z<<") -*- (" << tri.point3().x<<"," << tri.point3().y<<"," <<tri.point3().z <<std::endl;
    
    
    
    double area =abs(mal.length())/2;
    double al,be,ga,area1,area2;

    
    glm::vec3 eye(2,0,-0.1);//ray.origin
    glm::vec3 dir(-1,0,0);//ray.direction
    
    bool b;
    double t;
    if(dot(mal,dir)==0){b=0;}
    else{
        if(mal.x!=0){t=(tri.point1().x-eye.x)/dir.x;}
        else if(mal.y!=0){t=(tri.point1().y-eye.y)/dir.y;}
        else{t=(tri.point1().z-eye.z)/dir.y;}
        
        
        glm::vec3 sdir(t*-1,t*0,t*0);
        glm::vec3 pnt= eye+sdir;
        
        std::cout << "plane vector:" <<  pnt.x<<"," << pnt.y<<"," <<pnt.z << std::endl;
        
        glm::vec3 bside1 = tri.point2()-pnt;
        
        std::cout << "sub2:" <<  bside1.x<<"," << bside1.y<<"," <<bside1.z << std::endl;
        glm::vec3 bside2 = tri.point3()-pnt;
        std::cout << "sub2:" <<  bside2.x<<"," << bside2.y<<"," <<bside2.z << std::endl;
        glm::vec3 bmal = glm::cross(bside1, bside2);
        std::cout << "bmal:" <<  bmal.x<<"," << bmal.y<<"," <<bmal.z << std::endl;
        
        area1 = sqrt(bmal.x*bmal.x+bmal.y*bmal.y+bmal.z*bmal.z)/2;
        
        std::cout<< area1<<std::endl;
        
        glm::vec3 cside1 = tri.point1()-pnt;
        glm::vec3 cside2 = tri.point3()-pnt;
        glm::vec3 cmal = glm::cross(cside1, cside2);
        area2 = sqrt(cmal.x*cmal.x+cmal.y*cmal.y+cmal.z*cmal.z)/2;
        
        al = area1/area;
        be =area2/area;
        ga =1-al-be;
        
        if(al>0 && al<1 && be <1 && be>0 && ga<1 && ga>0){b=1;}
    }
    
    std::cout<< t << std::endl;
    std::cout<< b << std::endl;
    std::cout<< area <<","<<area1<<","<<area2<< std::endl;
    std::cout<< al<<","<< be<<","<< ga << std::endl;
    */
}


