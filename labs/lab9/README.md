## Brad's lab9

In the img folder there is a runtime.png that shows the runtime.

My strategy was to divide the world into two boxes. I updated the intersector to take in 3 worlds. world1 is the right half world2 is the left. I played with how much overlap between the left and right sides. When there was a large overlap there was not much improvement. When there was a small overlap there was a dermatic improvement in the runtime!
