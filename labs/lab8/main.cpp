#include <iostream>

#include <glm/glm.hpp>


#include <bitmap/bitmap_image.hpp>

class ray {
private:
    float xval;
    float yval;
    float zval;
    float dx;
    float dy;
    float dz;
    
public:
    
    ray(float x, float y, float z, float v1, float v2, float v3){
        xval = x;
        yval = y;
        zval = z;
        dx = v1;
        dy = v2;
        dz = v3;
    }
    
    float x() const {return xval;}
    float y() const {return yval;}
    float z() const {return zval;}
    float v1() const {return dx;}
    float v2() const {return dy;}
    float v3() const {return dz;}
   
    
    void  print() const {
        
        std::cout <<"center ("<< xval << "," << yval <<"," << zval <<  ") vector  ["<<dx<<","<<dy<<","<<dz<<"]"<<std::endl;
    }
    
 
    
};

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
    glm::vec3 c() const {return center;}
    float r() const {return radius;}
    glm::vec3 col() const {return color;}
};

float intersect(const ray r, const Sphere s){
    bool sec=1;
    glm::vec3 p=glm::vec3(r.x(),r.y(),r.z());
    glm::vec3 d=glm::vec3(r.v1(),r.v2(),r.v3());
    return dot(d,(p-s.c()))*dot(d,(p-s.c()))-(length(p-s.c())*length(p-s.c())-s.r()*s.r());
    
}

glm::vec2 pointz(const ray r, const Sphere s){
    
    glm::vec3 p=glm::vec3(r.x(),r.y(),r.z());
    glm::vec3 d=glm::vec3(r.v1(),r.v2(),r.v3());
    float dis =sqrt(dot(d,(p-s.c()))*dot(d,(p-s.c()))-(length(p-s.c())*length(p-s.c())-s.r()*s.r()));
    
    float first = -dot(d,(p-s.c()));
    glm::vec2 pz(first-dis,first+dis);
    return pz;
    
}
void render(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer
    
    
    for (std::size_t y = 0; y < 460; ++y)
    {
        for (std::size_t x = 0; x < 640; ++x)
        {
            /* orthographic
            int ui =(640)*(x+.5) / 640;
            int vj =(460)*(y+.5) / 460;
            int count=0;
            int object;
            float t1=0;
            float t2=1000;
            ray r (ui,vj,0,0,0,-1);
            for (unsigned i=0; i<world.size(); i++){
                if(intersect(r,world.at(i))>0){
                    count=count+1;
                    if(0< pointz(r,world.at(i)).x && pointz(r,world.at(i)).x< t2){
                        t2=pointz(r,world.at(i)).x;
                        image.set_pixel(x, y, world.at(i).col().x,world.at(i).col().y,world.at(i).col().z);
                    }
                    
                    
                }
            }
            
            if(count==0){image.set_pixel(x, y, 75,156,211);}
            */
            //perspective
            int ui =(x+.5);
            int vj =(y+.5);
            int count=0;
            int object;
            float t1=0;
            float t2=1000;
            float d =100;
            float norm = sqrt((x-ui)*(x-ui)+(y-vj)*(y-vj)+d*d);
            ray r (ui,vj,d,(x-ui)/norm,(y-vj)/norm,-d/norm);
            for (unsigned i=0; i<world.size(); i++){
                if(intersect(r,world.at(i))>0){
                    count=count+1;
                    if(0< pointz(r,world.at(i)).x && pointz(r,world.at(i)).x< t2){
                        t2=pointz(r,world.at(i)).x;
                        image.set_pixel(x, y, world.at(i).col().x,world.at(i).col().y,world.at(i).col().z);
                    }
                    
                    
                }
            }
            
            if(count==0){image.set_pixel(x, y, 215,156,211);}
        }
    }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(190, 190, -100), 50, glm::vec3(255,255,0)),//yellow
        Sphere(glm::vec3(210, 400, -440), 110, glm::vec3(0,255,255)),//blue
        Sphere(glm::vec3(200, 200, -400), 100, glm::vec3(255,0,255)),//pink
    };

    // render the world
    render(image, world);
    ray a (190,190,0,0,0,-1);
    Sphere p (glm::vec3(190, 190, -100), 50, glm::vec3(1,1,0));
    a.print();
    std::cout<< intersect(a,p)<<std::endl;
    
    std::cout<< pointz(a,p).x <<","<< pointz(a,p).y << std::endl;
    ray a1 (190,190,0,0,0,-1);
    Sphere p1 (glm::vec3(200, 200, -400), 100, glm::vec3(1,1,0));
    std::cout<< pointz(a1,p1).x <<","<< pointz(a1,p1).y << std::endl;
    image.save_image("ray-traced.bmp");
    std::cout << "Success" << std::endl;
}


