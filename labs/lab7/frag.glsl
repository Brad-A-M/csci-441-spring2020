#version 330 core

out vec4 fragColor;
in vec2 ourTexCoord;
/**
 * TODO: PART-1 update the fragment shader to get the texture coordinates from
 * the vertex shader
 */
uniform sampler2D ourTexture;

/**
 * TODO: PART-3 update the fragment shader to get the fragColor color from the
 * texture, and add the sampler2D.
 */

void main() {
    //vec4 color = texture({sampler2D variable}, {uv coordinate}))
   
    fragColor = texture(ourTexture, ourTexCoord);
}
