#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

#include "matrix3.h"

int count = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
        std::cout<< count <<std::endl;
    }
    else if(glfwGetKey(window, GLFW_KEY_SPACE)==GLFW_PRESS){
        count=count+1;
    }
}

int main(int argc, char **argv) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing*/
    
    float mat[9];
    for(int i=0;i<9;i++){
        mat[i]=0;
    }

    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);
        
        
        // render
        // clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        

        char key;
        
        
        // update shader uniform
        float timeValue = glfwGetTime();
       
        matrix3 transform=mat;
        matrix3 a=mat;
        matrix3 b=mat;
        matrix3 c=mat;
        matrix3 r=mat;
        if(count == 0){
            transform.set0(1);
            transform.set1(0);
            transform.set2(0);
            transform.set3(0);
            transform.set4(1);
            transform.set5(0);
            transform.set6(0);
            transform.set7(0);
            transform.set8(1);
        }else if(0<count && count<=6){
            transform.set0(cos(timeValue));
            transform.set1(sin(timeValue));
            transform.set2(0);
            transform.set3(-sin(timeValue));
            transform.set4(cos(timeValue));
            transform.set5(0);
            transform.set6(0);
            transform.set7(0);
            transform.set8(1);
        }else if(6<count && count<=12){
            a.set0(cos(timeValue-3));
            a.set1(sin(timeValue-3));
            a.set2(0);
            a.set3(-sin(timeValue-3));
            a.set4(cos(timeValue-3));
            a.set5(0);
            a.set6(0);
            a.set7(0);
            a.set8(1);
            b.set0(1);
            b.set1(0);
            b.set2(0);
            b.set3(0);
            b.set4(1);
            b.set5(0);
            b.set6(.5);
            b.set7(.5);
            b.set8(1);
            transform=a*b;
        }
        
        else if(12<count && count<=18){
            transform.set0(.1*timeValue/5);
            transform.set1(0);
            transform.set2(0);
            transform.set3(0);
            transform.set4(.1*timeValue/5);
            transform.set5(0);
            transform.set6(0);
            transform.set7(0);
            transform.set8(1);
        }
        else if(18<count){
            c.set0(1);
            c.set1(0);
            c.set2(0);
            c.set3(0);
            c.set4(1);
            c.set5(0);
            c.set6(timeValue*cos(timeValue)/30);
            c.set7(sin(timeValue));
            c.set8(1);
            
            r.set0(cos(timeValue-3));
            r.set1(sin(timeValue-3));
            r.set2(0);
            r.set3(-sin(timeValue-3));
            r.set4(cos(timeValue-3));
            r.set5(0);
            r.set6(0);
            r.set7(0);
            r.set8(1);
            transform= c*r*b;
        }
        // use the shader
        shader.use();
        unsigned int transformLoc = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix3fv(transformLoc, 1, GL_FALSE, transform.getfloats());
       

        
        /** Part 2 animate and scene by updating the transformation matrix */

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
