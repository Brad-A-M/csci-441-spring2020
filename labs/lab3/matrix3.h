#ifndef _MATRIX3_H_
#define _MATRIX3_H_

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <array>

/////matrix class ///////
class matrix3 {
public:
    float values[9];
   
    

    
    matrix3(float data[9]){
        //for(int i=0;i<9;i++){float values[i]=0;}
        values[0] = 1;
        values[1] = 0;
        values[2] = 0;
        values[3] = 0;
        values[4] = 1;
        values[5] = 0;
        values[6] = 0;
        values[7] = 0;
        values[8] = 1;
        
    }
    //getters
    float zero() const {return values[0];}
    float one() const {return values[1];}
    float two() const {return values[2];}
    float three() const {return values[3];}
    float four() const {return values[4];}
    float five() const {return values[5];}
    float six() const {return values[6];}
    float seven() const {return values[7];}
    float eight() const {return values[8];}
    
    //setters
    void set0(float f){values[0]=f;}
    void set1(float f){values[1]=f;}
    void set2(float f){values[2]=f;}
    void set3(float f){values[3]=f;}
    void set4(float f){values[4]=f;}
    void set5(float f){values[5]=f;}
    void set6(float f){values[6]=f;}
    void set7(float f){values[7]=f;}
    void set8(float f){values[8]=f;}
    
    float* getfloats() {
        
        return &values[0];
    }
    
    void  print() const {
        
        std::cout <<"("<< values[0] << "," << values[3] << ","<< values[6]<<")" << std::endl;
        std::cout <<"("<< values[1] << "," << values[4] << ","<< values[7]<<")" << std::endl;
        std::cout <<"("<< values[2] << "," << values[5] << ","<< values[8]<<")" << std::endl;
    }
};

matrix3 operator *(const matrix3 &a, const matrix3 &b){
    
    float z[9];
    
    matrix3 c=z;
    c.set0(a.zero()*b.zero()+a.three()*b.one()+a.six()*b.two());
    c.set1(a.one()*b.zero()+a.four()*b.one()+a.seven()*b.two());
    c.set2(a.two()*b.zero()+a.five()*b.one()+a.eight()*b.two());

    c.set3(a.zero()*b.three()+a.three()*b.four()+a.six()*b.five());
    c.set4(a.one()*b.three()+a.four()*b.four()+a.seven()*b.five());
    c.set5(a.two()*b.three()+a.five()*b.four()+a.eight()*b.five());
    
    c.set6(a.zero()*b.six()+a.three()*b.seven()+a.six()*b.eight());
    c.set7(a.one()*b.six()+a.four()*b.seven()+a.seven()*b.eight());
    c.set8(a.two()*b.six()+a.five()*b.seven()+a.eight()*b.eight());
    
              
              return c;
}


class vector3 {
public:
    float x;
    float y;
    float z;
    
    // Ve(int r = 0, int i =0)  {real = r;   imag = i;}
    // Constructor
    vector3(float xx=0, float yy=0, float zz=0){ x=xx; y=yy; z=zz;
        // nothing to do here as we've already initialized x, y, and z above
        //std::cout << "in Vector3 constructor" << std::endl;
    }
    ////geters/////
    float xval() const {return x;}
    float yval() const {return y;}
    float zval() const {return z;}
    
    //setters/////////
    void setx(float f){
        x=f;
    }
    void sety(float f){
        y=f;
    }
    void setz(float f){
        z=f;
    }
    
    void print() {
        
        std::cout << "("<< x << "," << y <<  "," << z << ")" << std::endl;
    }
    float lengthSquared() {
        return x*x+y*y+z*z;
    }
    
    
    
    // Destructor - called when an object goes out of scope or is destroyed
    ~vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        //std::cout << "in Vector3 destructor" << std::endl;
    }
};

vector3 operator+(const vector3 &v,const vector3 &v2){
    vector3 c(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    return c;
}

vector3 operator *(const matrix3 &a, const vector3 &v){
    vector3 c(a.zero()*v.xval()+a.three()*v.yval()+a.six()*v.zval(),
              a.one()*v.xval()+a.four()*v.yval()+a.seven()*v.zval(),
              a.two()*v.xval()+a.five()*v.yval()+a.eight()*v.zval());
    return c;
    
}

#endif
