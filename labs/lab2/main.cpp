#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>


/////point class ///////
class point {
private:
    float xval;
    float yval;
    
public:
    
    point(float x=0, float y=0){
        xval = x;
        yval = y;
    }
    
    float x() const {return xval;}
    float y() const {return yval;}
    
    void  print() const {
        
        std::cout <<"("<< xval << "," << yval << ")\n";
    }
};
////////////////////////////////////
// function to convert poins
point w2nd(point z){
    float x = -1 + z.x()*2/640;
    float y = 1- z.y()*2/480;
    point p(x,y);
    
    return p;
    
}
////////////////////////////////////

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */
void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */
    GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = 0;
    
    program = glCreateProgram();
    
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}




int main(void) {
    
    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */
    ////////////////////////
    //Input data from lab1//
    ////////////////////////
    
    char skip;
    float p1x, p1y, p2x, p2y, p3x, p3y;
    float p1ri, p1gi, p1bi, p2ri, p2gi, p2bi, p3ri, p3gi, p3bi;
    /*
    p1x=160; p1y=360; p1ri=1; p1gi=0; p1bi=0;
    p2x=480; p2y=360; p2ri=0; p2gi=1; p2bi=0;
    p3x=320; p3y=120; p3ri=0; p3gi=0; p3bi=1;
    
    
    */
    
    std::cout <<"Enter 3 points (enter a point as x,y:r,g,b):"<< std::endl;
    std::cout <<"First point:"<< std::endl;
    std::cin >> p1x >> skip >> p1y >> skip >> p1ri >> skip >> p1gi >> skip >> p1bi;
    
    std::cout <<"Point 1 is: ("<< p1x<<","<< p1y<<") : ("<< p1ri<<","<<p1gi<<","<<p1bi<<")"<< std::endl;
    
    std::cout <<"Second point:"<< std::endl;
    std::cin >> p2x >> skip >> p2y >> skip>> p2ri >> skip >> p2gi >> skip >> p2bi;
    
    std::cout <<"Point 2 is: ("<< p2x<<","<< p2y<<") : ("<< p2ri<<","<<p2gi<<","<<p2bi<<")"<< std::endl;
    
    std::cout <<"Thrid point:"<< std::endl;
    std::cin >> p3x >> skip >> p3y >> skip >> p3ri >> skip >> p3gi >> skip >> p3bi;
    
    std::cout <<"Point 3 is: ("<< p3x<<","<< p3y<<") : ("<< p3ri<<","<<p3gi<<","<<p3bi<<")"<< std::endl;
    
   
     
    point p1(p1x,p1y);
    point p2(p2x,p2y);
    point p3(p3x,p3y);
    point p1nd = w2nd(p1);
    point p2nd = w2nd(p2);
    point p3nd = w2nd(p3);
    ////////////////////////
    ///end data input
    ////////////////////////
    
    // create an image 640 pixels wide by 480 pixels tall
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }
   
    // convert the triangle to an array of floats containing
    // normalized device coordinates and color components.
    // float triangle[] = ...

    /** PART2: map the data */
    //points  //colors
    float vertices[] = {
        p1nd.x(), p1nd.y(), 0.0f, p1ri, p1gi, p1bi,
        p2nd.x(), p2nd.y(), 0.0f, p2ri, p2gi, p2bi,
        p3nd.x(),  p3nd.y(), 0.0f, p3ri, p3gi, p3bi
    };
    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    // GLuint VBO[1], VAO[1];
    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // setup the attribute pointer for the coordinates
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);
    
    
    //glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    //glEnableVertexAttribArray(0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;

    /** PART3: create the shader program */
    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);
    
    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);
    
    // cleanup the vertex and fragment shaders using glDeleteShader
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
        
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        
        glUseProgram(shaderProgram);
        glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
        glDrawArrays(GL_TRIANGLES, 0, 3);
        // set the shader program using glUseProgram

        // bind the vertex array using glBindVertexArray

        // draw the triangles using glDrawArrays


        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glfwTerminate();
    return 0;
}
