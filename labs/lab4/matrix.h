#ifndef _MATRIX3_H_
#define _MATRIX3_H_

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <array>
/////matrix class ///////
class matrix4 {
public:
    float values[16];
    
    matrix4(float data[16]){
        //for(int i=0;i<9;i++){float values[i]=0;}
        values[0] = data[0];
        values[1] = data[1];
        values[2] = data[2];
        values[3] = data[3];
        values[4] = data[4];
        values[5] = data[5];
        values[6] = data[6];
        values[7] = data[7];
        values[8] = data[8];
        values[9] = data[9];
        values[10] = data[10];
        values[11] = data[11];
        values[12] = data[12];
        values[13] = data[13];
        values[14] = data[14];
        values[15] = data[15];
        
    }
    //getters
    float zero() const {return values[0];}
    float one() const {return values[1];}
    float two() const {return values[2];}
    float three() const {return values[3];}
    float four() const {return values[4];}
    float five() const {return values[5];}
    float six() const {return values[6];}
    float seven() const {return values[7];}
    float eight() const {return values[8];}
    float nine() const {return values[9];}
    float ten() const {return values[10];}
    float eleven() const {return values[11];}
    float twelve() const {return values[12];}
    float thirteen() const {return values[13];}
    float fourteen() const {return values[14];}
    float fifteen() const {return values[15];}
    
    //setters
    void set0(float f){values[0]=f;}
    void set1(float f){values[1]=f;}
    void set2(float f){values[2]=f;}
    void set3(float f){values[3]=f;}
    void set4(float f){values[4]=f;}
    void set5(float f){values[5]=f;}
    void set6(float f){values[6]=f;}
    void set7(float f){values[7]=f;}
    void set8(float f){values[8]=f;}
    void set9(float f){values[9]=f;}
    void set10(float f){values[10]=f;}
    void set11(float f){values[11]=f;}
    void set12(float f){values[12]=f;}
    void set13(float f){values[13]=f;}
    void set14(float f){values[14]=f;}
    void set15(float f){values[15]=f;}
    
    float* getfloats() {
        
        return &values[0];
    }
    
    void  print() const {
        
        std::cout <<"("<< values[0] << "," << values[4] << ","<< values[8]<<","<<values[12]<<")" << std::endl;
        std::cout <<"("<< values[1] << "," << values[5] << ","<< values[9]<<","<<values[13]<< ")" << std::endl;
        std::cout <<"("<< values[2] << "," << values[6] << ","<< values[10]<<","<<values[14]<<")" << std::endl;
        std::cout <<"("<< values[3] << "," << values[7] << ","<< values[11]<<","<<values[15]<<")" << std::endl;
    }
};
matrix4 rotatex(const float theta){
    float z[16];
    matrix4 c=z;
    c.set0(1);
    c.set1(0);
    c.set2(0);
    c.set3(0);
    
    c.set4(0);
    c.set5(cos(theta));
    c.set6(sin(theta));
    c.set7(0);
    
    c.set8(0);
    c.set9(-sin(theta));
    c.set10(cos(theta));
    c.set11(0);
    
    c.set12(0);
    c.set13(0);
    c.set14(0);
    c.set15(1);
    
    return c;
}
matrix4 rotatey(const float theta){
    float z[16];
    matrix4 c=z;
    c.set0(cos(theta));
    c.set1(0);
    c.set2(sin(theta));
    c.set3(0);
    
    c.set4(0);
    c.set5(1);
    c.set6(0);
    c.set7(0);
    
    c.set8(-sin(theta));
    c.set9(0);
    c.set10(cos(theta));
    c.set11(0);
    
    c.set12(0);
    c.set13(0);
    c.set14(0);
    c.set15(1);
    
    return c;
}
matrix4 rotatez(const float theta){
    float z[16];
    matrix4 c=z;
    c.set0(cos(theta));
    c.set1(sin(theta));
    c.set2(0);
    c.set3(0);
    
    c.set4(-sin(theta));
    c.set5(cos(theta));
    c.set6(0);
    c.set7(0);
    
    c.set8(0);
    c.set9(0);
    c.set10(1);
    c.set11(0);
    
    c.set12(0);
    c.set13(0);
    c.set14(0);
    c.set15(1);
    
    return c;
}


matrix4 scale(const float size){
    float z[16];
    matrix4 c=z;
    c.set0(size);
    c.set1(0);
    c.set2(0);
    c.set3(0);
    
    c.set4(0);
    c.set5(size);
    c.set6(0);
    c.set7(0);
    
    c.set8(0);
    c.set9(0);
    c.set10(size);
    c.set11(0);
    
    c.set12(0);
    c.set13(0);
    c.set14(0);
    c.set15(1);
    
    return c;
}
matrix4 translate(const float x, const float y, const float z){
    float q[16];
    matrix4 c=q;
    c.set0(1);
    c.set1(0);
    c.set2(0);
    c.set3(0);
    
    c.set4(0);
    c.set5(1);
    c.set6(0);
    c.set7(0);
    
    c.set8(0);
    c.set9(0);
    c.set10(1);
    c.set11(0);
    
    c.set12(x);
    c.set13(y);
    c.set14(z);
    c.set15(1);
    
    return c;
}

matrix4 perspective(const float l, const float r, const float b, const float t, const float n, const float f){
    float q[16];
    matrix4 c=q;
    c.set0((2*n)/(r-l));
    c.set1(0);
    c.set2(0);
    c.set3(0);
    
    c.set4(0);
    c.set5((2*n)/(t-b));
    c.set6(0);
    c.set7(0);
    
    c.set8((r+l)/(r-l));
    c.set9((t+b)/(t-b));
    c.set10(-(f+n)/(f-n));
    c.set11(-1);
    
    c.set12(0);
    c.set13(0);
    c.set14((-2*f*n)/(f-n));
    c.set15(0);
    
    return c;
}

matrix4 orthog(const float l, const float r, const float b, const float t, const float n, const float f){
    float q[16];
    matrix4 c=q;
    c.set0((2)/(r-l));
    c.set1(0);
    c.set2(0);
    c.set3(0);
    
    c.set4(0);
    c.set5((2)/(t-b));
    c.set6(0);
    c.set7(0);
    
    c.set8(0);
    c.set9(0);
    c.set10(2/(n-f));
    c.set11(0);
    
    c.set12(-1*(r+l)/(r-l));
    c.set13(-1*(t+b)/(t-b));
    c.set14(((f+n))/(n-f));
    c.set15(1);
    
    return c;
}

matrix4 operator *(const matrix4 &a, const matrix4 &b){
    
    float z[16];
    
    matrix4 c=z;
    c.set0(a.zero()*b.zero()+a.four()*b.one()+a.eight()*b.two()+a.twelve()*b.three());
    c.set1(a.one()*b.zero()+a.five()*b.one()+a.nine()*b.two()+a.thirteen()*b.three());
    c.set2(a.two()*b.zero()+a.six()*b.one()+a.ten()*b.two()+a.fourteen()*b.three());
    c.set3(a.three()*b.zero()+a.seven()*b.one()+a.eleven()*b.two()+a.fifteen()*b.three());
    
    c.set4(a.zero()*b.four()+a.four()*b.five()+a.eight()*b.six()+a.twelve()*b.seven());
    c.set5(a.one()*b.four()+a.five()*b.five()+a.nine()*b.six()+a.thirteen()*b.seven());
    c.set6(a.two()*b.four()+a.six()*b.five()+a.ten()*b.six()+a.fourteen()*b.seven());
    c.set7(a.three()*b.four()+a.seven()*b.five()+a.eleven()*b.six()+a.fifteen()*b.seven());
    
    c.set8(a.zero()*b.eight()+a.four()*b.nine()+a.eight()*b.ten()+a.twelve()*b.eleven());
    c.set9(a.one()*b.eight()+a.five()*b.nine()+a.nine()*b.ten()+a.thirteen()*b.eleven());
    c.set10(a.two()*b.eight()+a.six()*b.nine()+a.ten()*b.ten()+a.fourteen()*b.eleven());
    c.set11(a.three()*b.eight()+a.seven()*b.nine()+a.eleven()*b.ten()+a.fifteen()*b.eleven());
    
    c.set12(a.zero()*b.twelve()+a.four()*b.thirteen()+a.eight()*b.fourteen()+a.twelve()*b.fifteen());
    c.set13(a.one()*b.twelve()+a.five()*b.thirteen()+a.nine()*b.fourteen()+a.thirteen()*b.fifteen());
    c.set14(a.two()*b.twelve()+a.six()*b.thirteen()+a.ten()*b.fourteen()+a.fourteen()*b.fifteen());
    c.set15(a.three()*b.twelve()+a.seven()*b.thirteen()+a.eleven()*b.fourteen()+a.fifteen()*b.fifteen());
    
    
    return c;
}

matrix4 camera1(const float h){
    float z[16];
    matrix4 c=z;
    matrix4 e=z;
    matrix4 r=z;
    int fty=20;
    float g=sqrt(h*h+fty*fty);
    e.set0(-1);
    e.set1(0);
    e.set2(0);
    e.set3(0);
    
    e.set4(0);
    e.set5(fty/g);
    e.set6(-h/g);
    e.set7(0);
    
    e.set8(0);
    e.set9(h/g);
    e.set10(fty/g);
    e.set11(0);
    
    e.set12(0);
    e.set13(0);
    e.set14(0);
    e.set15(1);
    
    
    r.set0(1);
    r.set1(0);
    r.set2(0);
    r.set3(0);
    
    r.set4(0);
    r.set5(1);
    r.set6(0);
    r.set7(0);
    
    r.set8(0);
    r.set9(0);
    r.set10(1);
    r.set11(0);
    
    r.set12(0);
    r.set13(-h);
    r.set14(-fty);
    r.set15(1);
    
    return c=e*r;
}
/////matrix class ///////
class matrix3 {
public:
    float values[9];
   
    

    
    matrix3(float data[9]){
        //for(int i=0;i<9;i++){float values[i]=0;}
        values[0] = 1;
        values[1] = 0;
        values[2] = 0;
        values[3] = 0;
        values[4] = 1;
        values[5] = 0;
        values[6] = 0;
        values[7] = 0;
        values[8] = 1;
        
    }
    //getters
    float zero() const {return values[0];}
    float one() const {return values[1];}
    float two() const {return values[2];}
    float three() const {return values[3];}
    float four() const {return values[4];}
    float five() const {return values[5];}
    float six() const {return values[6];}
    float seven() const {return values[7];}
    float eight() const {return values[8];}
    
    //setters
    void set0(float f){values[0]=f;}
    void set1(float f){values[1]=f;}
    void set2(float f){values[2]=f;}
    void set3(float f){values[3]=f;}
    void set4(float f){values[4]=f;}
    void set5(float f){values[5]=f;}
    void set6(float f){values[6]=f;}
    void set7(float f){values[7]=f;}
    void set8(float f){values[8]=f;}
    
    float* getfloats() {
        
        return &values[0];
    }
    
    void  print() const {
        
        std::cout <<"("<< values[0] << "," << values[3] << ","<< values[6]<<")" << std::endl;
        std::cout <<"("<< values[1] << "," << values[4] << ","<< values[7]<<")" << std::endl;
        std::cout <<"("<< values[2] << "," << values[5] << ","<< values[8]<<")" << std::endl;
    }
};

matrix3 operator *(const matrix3 &a, const matrix3 &b){
    
    float z[9];
    
    matrix3 c=z;
    c.set0(a.zero()*b.zero()+a.three()*b.one()+a.six()*b.two());
    c.set1(a.one()*b.zero()+a.four()*b.one()+a.seven()*b.two());
    c.set2(a.two()*b.zero()+a.five()*b.one()+a.eight()*b.two());

    c.set3(a.zero()*b.three()+a.three()*b.four()+a.six()*b.five());
    c.set4(a.one()*b.three()+a.four()*b.four()+a.seven()*b.five());
    c.set5(a.two()*b.three()+a.five()*b.four()+a.eight()*b.five());
    
    c.set6(a.zero()*b.six()+a.three()*b.seven()+a.six()*b.eight());
    c.set7(a.one()*b.six()+a.four()*b.seven()+a.seven()*b.eight());
    c.set8(a.two()*b.six()+a.five()*b.seven()+a.eight()*b.eight());
    
              
              return c;
}


class vector3 {
public:
    float x;
    float y;
    float z;
    
    // Ve(int r = 0, int i =0)  {real = r;   imag = i;}
    // Constructor
    vector3(float xx=0, float yy=0, float zz=0){ x=xx; y=yy; z=zz;
        // nothing to do here as we've already initialized x, y, and z above
        //std::cout << "in Vector3 constructor" << std::endl;
    }
    ////geters/////
    float xval() const {return x;}
    float yval() const {return y;}
    float zval() const {return z;}
    
    //setters/////////
    void setx(float f){
        x=f;
    }
    void sety(float f){
        y=f;
    }
    void setz(float f){
        z=f;
    }
    
    void print() {
        
        std::cout << "("<< x << "," << y <<  "," << z << ")" << std::endl;
    }
    float lengthSquared() {
        return x*x+y*y+z*z;
    }
    
    
    
    // Destructor - called when an object goes out of scope or is destroyed
    ~vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        //std::cout << "in Vector3 destructor" << std::endl;
    }
};

vector3 operator+(const vector3 &v,const vector3 &v2){
    vector3 c(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    return c;
}

vector3 operator *(const matrix3 &a, const vector3 &v){
    vector3 c(a.zero()*v.xval()+a.three()*v.yval()+a.six()*v.zval(),
              a.one()*v.xval()+a.four()*v.yval()+a.seven()*v.zval(),
              a.two()*v.xval()+a.five()*v.yval()+a.eight()*v.zval());
    return c;
    
}

#endif
