#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
int mode=0;
float w=0;
float z=0;
float x=0;
float y=0;
float p=0;
float t=0;
float size=1;
void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}
bool is_pressed=false;
void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
        std::cout<<size<<std::endl;
    }
    
    else if(is_pressed==false && glfwGetKey(window, GLFW_KEY_BACKSLASH)==GLFW_PRESS){
        is_pressed=true;
        mode=mode+1;
    }
    if(is_pressed==true && glfwGetKey(window, GLFW_KEY_BACKSLASH)!=GLFW_PRESS){
        is_pressed=false;
    }
    
    else if(glfwGetKey(window, GLFW_KEY_BACKSLASH)==GLFW_PRESS){
        mode=mode+1;}
    //move y part 1
    else if(glfwGetKey(window, GLFW_KEY_W)==GLFW_PRESS){
        if(w<1000)
        w=w+1;}
    else if(glfwGetKey(window, GLFW_KEY_S)==GLFW_PRESS){
        if(w>-1000)
        w=w-1;}
    //move on y
    else if(glfwGetKey(window, GLFW_KEY_UP)==GLFW_PRESS){
        w=w+1;}
    else if(glfwGetKey(window, GLFW_KEY_DOWN)==GLFW_PRESS){
        w=w-1;}
    //rotate around z
    else if(glfwGetKey(window, GLFW_KEY_LEFT_BRACKET)==GLFW_PRESS){
        z=z+1;}
    else if(glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET)==GLFW_PRESS){
        z=z-1;}
    
    //rotate about  x
    else if(glfwGetKey(window, GLFW_KEY_U)==GLFW_PRESS){
        x=x+1;}
    else if(glfwGetKey(window, GLFW_KEY_I)==GLFW_PRESS){
        x=x-1;}
    //rotate about y
    else if(glfwGetKey(window, GLFW_KEY_O)==GLFW_PRESS){
        y=y+1;}
    else if(glfwGetKey(window, GLFW_KEY_P)==GLFW_PRESS){
        y=y-1;}
    //move on x
    else if(glfwGetKey(window, GLFW_KEY_RIGHT)==GLFW_PRESS){
        t=t+1;}
    else if(glfwGetKey(window, GLFW_KEY_LEFT)==GLFW_PRESS){
        t=t-1;}
    //move on z
    else if(glfwGetKey(window, GLFW_KEY_PERIOD)==GLFW_PRESS){
        p=p+1;}
    else if(glfwGetKey(window, GLFW_KEY_COMMA)==GLFW_PRESS){
        p=p-1;}
    //scale
    else if(glfwGetKey(window, GLFW_KEY_MINUS)==GLFW_PRESS){
        size=size*.9;}
    else if(glfwGetKey(window, GLFW_KEY_EQUAL)==GLFW_PRESS){
        size=(size*1.1);}
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };
    
    //make the cyliner
    int gon = 25;
    const float pie = 3.1415926563;
  
    std::vector<float> cy;
    //gree sides
    for(int i=0; i<gon;i++){
        
        cy.push_back(cos(2*pie*i/gon));
        cy.push_back(-0.5f);
        cy.push_back(sin(2*pie*i/gon));
        cy.push_back(0.0f);
        cy.push_back(0.8f);
        cy.push_back(0.0f);
        
        cy.push_back(cos(2*pie*(i+1)/gon));
        cy.push_back(-0.5f);
        cy.push_back(sin(2*pie*(i+1)/gon));
        cy.push_back(0.0f);
        cy.push_back(0.8f);
        cy.push_back(0.0f);
        
        cy.push_back(cos(2*pie*i/gon));
        cy.push_back(0.5f);
        cy.push_back(sin(2*pie*i/gon));
        cy.push_back(0.0f);
        cy.push_back(0.8f);
        cy.push_back(0.0f);
    
    }
    
    //red sides
    for(int i=0; i<gon;i++){
        cy.push_back(cos(2*pie*(i+1)/gon));
        cy.push_back(-0.5f);
        cy.push_back(sin(2*pie*(i+1)/gon));
        cy.push_back(8.0f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        
        cy.push_back(cos(2*pie*(i+1)/gon));
        cy.push_back(0.5f);
        cy.push_back(sin(2*pie*(i+1)/gon));
        cy.push_back(8.0f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        
        cy.push_back(cos(2*pie*i/gon));
        cy.push_back(0.5f);
        cy.push_back(sin(2*pie*i/gon));
        cy.push_back(8.0f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        
    }
    //bottom
    for(int i=0; i<gon;i++){
        cy.push_back(cos(2*pie*(i+1)/gon));
        cy.push_back(-0.5f);
        cy.push_back(sin(2*pie*(i+1)/gon));
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        
        cy.push_back(cos(2*pie*i/gon));
        cy.push_back(-0.5f);
        cy.push_back(sin(2*pie*i/gon));
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        
        cy.push_back(0.0f);
        cy.push_back(-0.5f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        
    }
    //top
    for(int i=0; i<gon;i++){
        cy.push_back(cos(2*pie*(i+1)/gon));
        cy.push_back(0.5f);
        cy.push_back(sin(2*pie*(i+1)/gon));
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        cy.push_back(1.0f);
        
        cy.push_back(cos(2*pie*i/gon));
        cy.push_back(0.5f);
        cy.push_back(sin(2*pie*i/gon));
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        cy.push_back(1.0f);
        
        cy.push_back(0.0f);
        cy.push_back(0.5f);
        cy.push_back(0.0f);
        cy.push_back(0.0f);
        cy.push_back(1.0f);
        cy.push_back(1.0f);
        
    }
        
 
    
    int length = cy.size();
    float cylinder[length];
    int run=0;
    for (std::vector<float>::iterator it = cy.begin() ; it != cy.end(); ++it){
         cylinder[run]=*it;
        run = run+1;
    }
    //done makeing cyliner

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylinder), cylinder, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    //shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    
    ///////////////////
    float mat[16];
    for(int i=0;i<16;i++){
        mat[i]=0;
    }
    matrix4 a=mat;
    matrix4 b=mat;
    a.set4(3);
    a.set0(4);
    b.set2(5);
    b.set3(5);
    matrix4 c=b*a;
    a.print();
    b.print();
    c.print();
    //////////////////
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

       
        //build transform
        matrix4 transform =mat;
        transform.set0(1);
        transform.set1(0);
        transform.set2(0);
        transform.set3(0);
        transform.set4(0);
        transform.set5(1);
        transform.set6(0);
        transform.set7(0);
        transform.set8(0);
        transform.set9(0);
        transform.set10(1);
        transform.set11(0);
        transform.set12(0);
        transform.set13(0);
        transform.set14(0);
        transform.set15(1);
        //part 1.
        //transform=transform*camera1(w/50)*scale(size);
        
        //part 2.
        //transform=transform*translate(t/50,w/50,p/50)*scale(.5*size)*rotatez(z/10)*rotatex(x/10)*rotatey(y/10);
        
        //part 3.
       //perspective(left,right,bottom,top,near,far)
       
        if(mode % 12 < 6){
            transform=transform*perspective(-10,10,-10,10,10,100)*camera1(w/50)*scale(size)*rotatez(z/10)*translate(t/50,w/50,p/50)*rotatex(x/10)*rotatey(y/10);}
        else{transform=transform*orthog(-10,10,-10,10,10,100)*camera1(w/50)*scale(size)*translate(t/50,w/50,p/50)*rotatez(z/10)*rotatex(x/10)*rotatey(y/10);}
       
     
        
        // activate shader
        shader.use();
        unsigned int transformLoc = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, transform.getfloats());
        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
