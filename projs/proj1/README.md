# Brad's Maze Exploitation

## Summary

When ./proj01 is run, we see the maze and our character (a sphere) from a birds eye view. Pressing the space bar switches between the character view and the birds eye view.

To move the character use up/down to move forward and back. Use left and right to rotate. The movements are most natural from the character view. To move I suggest to look at the characters postion in birds eye view, then do the moving in character view.

I used the shading from the lab6 code.

In the build file you will find ./proj01. This executable contains the maze game.

Have fun!
Brad
