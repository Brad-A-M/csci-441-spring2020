#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix4.h>
#include <csci441/matrix3.h>
#include <csci441/vector4.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
int normalls=0;
float up = 0.0;
float lat =0.0;
float theta = 0.0;




void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

//updated to include theta
Matrix4 processModel(const Matrix4& model, GLFWwindow *window,float theta) {
    Matrix4 trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;


    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(sin(theta)/100, 0,-cos(theta)/100); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(-sin(theta)/100, 0,cos(theta)/100); }
 

    return trans * model;
}

void processInput(Matrix4& model, GLFWwindow *window, float theta) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    
    model = processModel(model, window, theta);
    
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        normalls=normalls+1;
    }
}
void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//////////////////my move functions/////
Vector4 updownvec(GLFWwindow *window, float x, float y, float theta){
    
    
    if (isPressed(window, GLFW_KEY_UP)) {
        x=x+sin(theta)/100;
        y=y-cos(theta)/100;
    }
    else if (isPressed(window, GLFW_KEY_DOWN)) {
        x=x-sin(theta)/100;
        y=y+cos(theta)/100; }
    
    Vector4 a(x,y,0);
    return a;
}


float tate(GLFWwindow *window, float p){
    if (isPressed(window,GLFW_KEY_LEFT)) { p=p-0.02; }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { p=p+0.02; }
    
    
    return p;
}
//////////////////end my update functions/////


int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
   
    
    std::vector<float> sphere1 = Sphere(20,.1,.1,.1,.7).coords;
    //shift sphere to desired starting point
    std::vector<float> sphere;
    int countt = 0;
    for (std::vector<float>::iterator it = sphere1.begin() ; it != sphere1.end(); ++it){
        if(countt % 9==0){
            sphere.push_back(*it-2.2);}
        else if(countt % 9 == 2){sphere.push_back(*it+2.7);}
        else{sphere.push_back(*it);}
        countt=countt+1;
    }
    
    std::vector<float> maze = Maze().coords;
    
    
    Model obj1(
               sphere,
               Shader("../vert.glsl", "../frag.glsl"));
    Model mazze(
               maze,
               Shader("../vert.glsl", "../frag.glsl"));

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix4 floor_trans, floor_scale;
    floor_trans.translate(0, -0.5, 0);
    floor_scale.scale(100, 1, 100);
    floor.model = floor_trans*floor_scale;

    // setup camera
    Matrix4 projection;
    projection.perspective(45, 1, .01, 10);
    Camera camera;
    camera.projection = projection;
    camera.eye = Vector4(0, 6, 3);
    camera.origin = Vector4(0, 0, 0);
    camera.up = Vector4(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector4 lightPos(3.75f, 3.75f, 4.0f);

    //start position
    float x1=-2.2;
    float z1=2.7;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        
        
        theta=tate(window,theta);
        Camera camera1;
        camera1.projection = projection;
        //update camera1 when arrows are pushed
        x1=updownvec(window,x1,z1,theta).x();
        z1=updownvec(window,x1,z1,theta).y();
       
        camera1.eye = Vector4(x1, .1, z1);
        camera1.origin = Vector4(sin(theta)+x1, .1, -cos(theta)+z1);
        camera1.up = Vector4(0, 1, 0);
        //updateNormals(window);
        
        glfwSetKeyCallback(window, key_callback);
       
        // process input
        processInput(obj1.model, window,theta);
       
    
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object, the maze and the floor
        if(normalls%2==0){
            
            renderer.render(camera, obj1, lightPos);
            renderer.render(camera, floor, lightPos);
            renderer.render(camera, mazze, lightPos);
        }else{renderer.render(camera1, obj1, lightPos);
            renderer.render(camera1, floor, lightPos);
            renderer.render(camera1, mazze, lightPos);
        }
        

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
