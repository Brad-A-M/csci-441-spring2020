# Ben and Brad's Project 2

To run this project:
1) Move into the build directory
2) Type cmake ..
3) make
4) ./proj02

View output in ray-traced.bmp.
We chose to implement reflections, so every ball reflects the balls which neighbor it. We also implemented four procedural textures alongside our generic smooth texture. These textures include:

- Basic Smooth

[smooth texture](https://bitbucket.org/Brad-A-M/csci-441-spring2020/src/master/projs/proj2/img/smooth-ray-traced.bmp)

- Basic roughness (sandpaper texture)

[sandpaper texture](https://bitbucket.org/Brad-A-M/csci-441-spring2020/src/master/projs/proj2/img/basic-roughness-ray-traced.bmp)

- Zebra print

[zebra texture](https://bitbucket.org/Brad-A-M/csci-441-spring2020/src/master/projs/proj2/img/zebra-ray-traced.bmp)

- Deep Blue (waviness like our balls are deep underwater)

[deep blue texture](https://bitbucket.org/Brad-A-M/csci-441-spring2020/src/master/projs/proj2/img/deep-blu-ray-traced.bmp)

- Standard Waviness

[wavy texture](https://bitbucket.org/Brad-A-M/csci-441-spring2020/src/master/projs/proj2/img/wavy-ray-traced.bmp)

to change textures, go to line 138 in renderer.h and comment out lines as suggested for the desired texture
