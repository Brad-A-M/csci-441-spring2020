#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#include "camera.h"
#include "hit.h"
#include "light.h"
#include "ray.h"
#include "intersector.h"

int i;
int count;
int xoff;
int yoff;

class Renderer {
    

    Intersector* _intersector;

    int clamp255(float v) {
        return std::max(0.0f, std::min(255.0f, 255.0f*v));
    }


    rgb_t to_color(const glm::vec3 c) {
        return make_colour(clamp255(c.x), clamp255(c.y), clamp255(c.z));
    }


    glm::vec3 phong(const Hit& hit, const Light* light, const glm::vec3& eye) {

        float ambientStrength = 0.3;
        float specularStrength = 0.8;
        float shinyness = 4;

        glm::vec3 pos = hit.position();
        glm::vec3 normal = hit.normal();

        glm::vec3 light_dir = glm::normalize(light->direction(pos));

        float ambient = ambientStrength;

        float diffuse = glm::max(glm::dot(normal, light_dir), 0.0f);

        glm::vec3 view_dir = glm::normalize(eye - pos);
        glm::vec3 reflectDir = glm::reflect(-light_dir, normal);
        float specular = specularStrength *
            pow(std::fmax(glm::dot(view_dir, reflectDir), 0.0), shinyness);

        glm::vec3 light_color  =
            (ambient+diffuse+specular)
            * light->attenuation(pos)
            * light->color();

        return light_color*hit.color();
    }

    
    double rand_val() {
        static bool init = true;
        if (init) {
            srand(time(NULL));
            init = false;
        }
        return ((double) rand() / (RAND_MAX))/5;
    }
    
    double zebra_rand_val(){
        if (i < 30) {
            i++;
            return 0;
        }
        else if (i < 40){
            i++;
            return 1;
        }
        else{
            i = 0;
            return 0;
        }
    }
    
    double deep_blu_rand_val(){
        xoff = 10;
        
        if (i < xoff-2) {
            i++;
            return (1- i/xoff);
        }
        else{
            i = 0;
            return 0;
        }
        
    }
    
    double thinstripes(){
        if (i < 5) {
            i++;
            return 0;
        }
        else if (i < 8){
            i++;
            return rand_val();
        }
        else{
            i = 0;
            return 0;
        }

    }
    
    glm::vec3 sand_rand_color() {
        return glm::vec3(rand_val(),rand_val(),rand_val());
    }
    
    glm::vec3 zebra_rand_color(){
        return glm::vec3(zebra_rand_val(), zebra_rand_val(), zebra_rand_val());
    }
    
    glm::vec3 deep_blu_rand_color(){
        return glm::vec3(deep_blu_rand_val(), deep_blu_rand_val(), deep_blu_rand_val());
    }
   
    glm::vec3 thinstripes_color(){
        return glm::vec3(thinstripes(), thinstripes(), thinstripes());
    }
    
    glm::vec3 shade(const Camera& camera, const Lights& lights, const Hit& hit, const bool& marb) {
        glm::vec3 color = camera.background;
        if (hit.is_intersection()) {
            color = glm::vec3(0,0,0);
            for (auto light : lights) {
                color += phong(hit, light, camera.pos);
            }
        }
        
        // CHANGE TEXTURE HERE!!!!!!!!!
        //uncomment return line for desired texture
        if (marb) {
            //smooth
            return color;
            
            //sand
            //return color += sand_rand_color();
            
            //zebra
            //return color -= zebra_rand_color();
            
            //deep blue
            //return color -= deep_blu_rand_color();
            
            //standard wavy
            //return color += thinstripes_color();
            
        }
        else{
            return color;
        }
    }

    glm::vec3 render_pixel(
        const Camera& camera,
        const Lights& lights,
        const World& world,
        const Ray& ray
    ) {
        
        Hit hit = _intersector->find_first_intersection(world, ray);
        
        glm::vec3 color = camera.background;
        if (hit.is_intersection()) {
            color = glm::vec3(0,0,0);
            
            glm::vec3 pos = hit.position();
            glm::vec3 normal = hit.normal();
            
            glm::vec3 aye;
            aye = camera.pos;
            glm::vec3 I = glm::normalize((pos-aye));
            glm::vec3 reflectDir = I - normal*2.0f*dot(I, normal);
            reflectDir = glm::normalize(reflectDir);
            Ray ray1;
            ray1.origin = pos;
            ray1.direction=reflectDir;
            
            
            
            Hit hit1 =_intersector->find_first_intersection(world, ray1);
            if(hit1.is_intersection()){
                std::cout<<hit1._shape->_marbled<<std::endl;
                return shade(camera, lights, hit1, true);
            }
            else{
                std::cout<<hit._shape->_marbled<<std::endl;
                return shade(camera, lights, hit, true);}
        }
        
        return shade(camera, lights, hit, false);
        //return color;
    }

public:

    Renderer(Intersector* intersector) : _intersector(intersector) { }

    void render(
        bitmap_image &image,
        const Camera& camera,
        const Lights& lights,
        const World& world
    ) {
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                Ray ray = camera.make_ray(image.width(), image.height(), x, y);
                glm::vec3 c = render_pixel(camera, lights, world, ray);
                image.set_pixel(x, image.height()-y-1, to_color(c));
            }
        }
    }
};

#endif
